#include "reactor.hpp"

namespace {
    float calculateRatio(Cube_Common::Steps_t steps) {
        if (steps.a == 0 || steps.b == 0) {
            return 1.f;
        }
        int32_t abs_a = abs(steps.a);
        int32_t abs_b = abs(steps.b);
        if (abs_a > abs_b) {
            return static_cast<float>(abs_b)/abs_a;
        } else {
            return static_cast<float>(abs_a)/abs_b;
        }
    }
}


std::optional<Cube_Common::EncodedData_t> Reactor::doMove(Cube_Common::Command_t cmd) {
    if (!cmd.position) {
        return encoder.encodeReturn(Commands_MOVETO, Errors_MISC_ERROR, 2, nullptr, nullptr);
    }

    Cube_Core::PlannerResult_t planResult = planner.moveTo(*cmd.position);
    if (planResult.error != Cube_Core::PlannerErrors::noError) {
        return encoder.encodeReturn(Commands_MOVETO, Errors_PLANNER_ERROR, 1, nullptr, nullptr);
    }
    Cube_Common::Steps_t steps = *planResult.steps;
    Cube_Common::Steps_t relSteps = *planResult.relSteps;
    if (!steppers.configValid) {
        return encoder.encodeReturn(Commands_MOVETO, Errors_STEPPER_ERROR, 3, nullptr, nullptr);
    }

    float ratio = calculateRatio(relSteps);
    uint16_t ax1, ax2;
    uint16_t ax3 = Cube_Config::tmc429.steppers[2].v_max;
    if (abs(relSteps.a) > abs(relSteps.b)) {
        ax1 = Cube_Config::tmc429.steppers[0].v_max;
        ax2 = Cube_Config::tmc429.steppers[1].v_max * ratio;
    } else {
        ax1 = Cube_Config::tmc429.steppers[0].v_max * ratio;
        ax2 = Cube_Config::tmc429.steppers[1].v_max;
    }
    if (ax1 == 0) {
        ax1 = 1;
    }
    if (ax2 == 0) {
        ax2 = 2;
    }

    TMC429_Driver::Status temp = steppers.setMaxSpeed(ax1, ax2, ax3);
    if (temp != TMC429_Driver::Status::success) {
        return encoder.encodeReturn(Commands_MOVETO, Errors_STEPPER_ERROR, 8, nullptr, nullptr);
    }

    temp = steppers.doSteps(steps.a, steps.b, steps.z);
    if ( temp != TMC429_Driver::Status::success) {
        return encoder.encodeReturn(Commands_MOVETO, Errors_STEPPER_ERROR, 2, nullptr, nullptr);
    }
    TMC429_Driver::StepperStatus status = steppers.getStepperStatus();
    while (!status.stepper1_pos || !status.stepper2_pos || !status.stepper3_pos) {
        status = steppers.getStepperStatus();
        if (status.status != TMC429_Driver::Status::success) {
            return encoder.encodeReturn(Commands_MOVETO, Errors_STEPPER_ERROR, 1, nullptr, nullptr);
        }
    }
    return {};
}


void Reactor::moveInstruction(Cube_Common::Command_t cmd) {
    motorPower(1);
    
    auto err = doMove(cmd);
    if (err) {
        motorPower(0);
        sender.sendData(*err);
    }

    sendReturn(Commands_MOVETO, Errors_NO_ERROR, 0, nullptr, nullptr);
    motorPower(0);
}

void Reactor::homeInstruction(void) {
    Cube_Common::Command_t temp;
    temp.instruction = Cube_Common::Instruction::home;
    temp.position = {10, 10, 10};
    
    planner.setZeroPosition();

    motorPower(1);

    auto err = doMove(temp);
    if (err) {
        motorPower(0);
        sender.sendData(*err);
    }

    TMC429_Driver::Status status = steppers.setMode(TMC429_Driver::RampMode::VELOCITY);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 4, nullptr, nullptr);
        motorPower(0);
        return;
    }

    status = steppers.setSpeed(-Cube_Config::XY_HOMING, Cube_Config::XY_HOMING, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }
    while (GPIO_PinRead(CUBE_Y_LIMIT_GPIO, CUBE_Y_LIMIT_PIN) != 0);
    status = steppers.setSpeed(0, 0, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }

    status = steppers.setSpeed(-Cube_Config::XY_HOMING, -Cube_Config::XY_HOMING, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }
    while (GPIO_PinRead(CUBE_X_LIMIT_GPIO, CUBE_X_LIMIT_PIN) != 0);
    status = steppers.setSpeed(0, 0, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }

    status = steppers.setSpeed(0, 0, -Cube_Config::Z_HOMING);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }
    while (GPIO_PinRead(CUBE_Z_LIMIT_GPIO, CUBE_Z_LIMIT_PIN) != 0);
    status = steppers.setSpeed(0, 0, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 7, nullptr, nullptr);
        motorPower(0);
        return;
    }

    planner.resetPosition();
    status = steppers.setPosition(0, 0, 0);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 6, nullptr, nullptr);
        motorPower(0);
        return;
    }
    status = steppers.setMode(TMC429_Driver::RampMode::POSITION);
    if (status != TMC429_Driver::Status::success) {
        sendReturn(Commands_HOME, Errors_STEPPER_ERROR, 5, nullptr, nullptr);
        motorPower(0);
        return;
    }
    sendReturn(Commands_HOME, Errors_NO_ERROR, 0, nullptr, nullptr);
    motorPower(0);
}

void Reactor::measureInstruction(void) {
    motorPower(0);
    if (!sensor.configValid) {
        sendReturn(Commands_MEASURE, Errors_SENSOR_ERROR, 1, nullptr, nullptr);
        return;
    }
    sensor.triggerMeasurement(0x0F);

    sensor.measureDelay();

    MLX90393_Driver::RawResult rawData = sensor.getMeasurement();
    if (rawData.result_status != MLX90393_Driver::Status::success) {
        sendReturn(Commands_MEASURE, Errors_SENSOR_ERROR, 2, nullptr, nullptr);
        return;
    }
    MLX90393_Driver::Result data = sensor.convertRawResult(rawData);
    Cube_Common::MeasurementData_t measure;
    Cube_Common::Position_t pos = planner.getRelativePosition();
    measure.x = data.y;
    measure.y = data.x;
    measure.z = -data.z;
    sendReturn(Commands_MEASURE, Errors_NO_ERROR, 0, &pos, &measure);

}


void Reactor::statusInstruction(void) {
    if (!steppers.configValid) {
       sendReturn(Commands_STATUS, Errors_STEPPER_ERROR, 3, nullptr, nullptr); 
        return;
    }
    if (!sensor.configValid) {
        sendReturn(Commands_STATUS, Errors_SENSOR_ERROR, 1, nullptr, nullptr);
        return;
    }
    sendReturn(Commands_STATUS, Errors_NO_ERROR, 0, nullptr, nullptr);
}


void Reactor::doCommand(Cube_Common::Command_t cmd) {
    using instr = Cube_Common::Instruction;

    switch (cmd.instruction) 
    {
    case instr::getAbsolutePosition:
        {
            Cube_Common::Position_t pos = planner.getAbsolutePosition();
            sendReturn(Commands_GET_ABS_POS, Errors_NO_ERROR, 0, &pos, nullptr);
        }
        break;
    
    case instr::getRelativePosition:
        {
            Cube_Common::Position_t pos = planner.getRelativePosition();
            sendReturn(Commands_GET_REL_POS, Errors_NO_ERROR, 0, &pos, nullptr);
        }
        break;

    case instr::setZeroPosition:
        planner.setZeroPosition();
        sendReturn(Commands_SET_ZERO_POS, Errors_NO_ERROR, 0, nullptr, nullptr);
        break;

    case instr::measure:
        measureInstruction();
        break;
    
    case instr::moveTo:
        moveInstruction(cmd);
        break;
    
    case instr::raiseDecodeError:
        sendReturn(Commands_STATUS, Errors_MISC_ERROR, 1, nullptr, nullptr);
        break;
    
    case instr::status:
        statusInstruction();
        break;
    
    case instr::home:
        homeInstruction();
        break;
    case instr::unknown:
        sendReturn(Commands_UNKNOWN, Errors_MISC_ERROR, 0, nullptr, nullptr);
        break;
    
    case instr::nop:
    default:
        break;
    }
}