#include "peripherals.h"
#include "pin_mux.h"
#include "fsl_gpio.h"

#include "cube_common.hpp"
#include "cube_config.hpp"
#include "planner.hpp"
#include "serial_encoder.hpp"
#include "serial_sender.hpp"
#include "TMC429.hpp"
#include "MLX90393.hpp"


inline void motorPower(uint8_t on_off) {
    GPIO_PinWrite(CUBE_A_ENABLE_GPIO, CUBE_A_ENABLE_PIN, on_off);
    GPIO_PinWrite(CUBE_B_ENABLE_GPIO, CUBE_B_ENABLE_PIN, on_off);
    GPIO_PinWrite(CUBE_Z1_ENABLE_GPIO, CUBE_Z1_ENABLE_PIN, on_off);
    GPIO_PinWrite(CUBE_Z2_ENABLE_GPIO, CUBE_Z2_ENABLE_PIN, on_off);
}


class Reactor {
    Cube_Core::SerialEncoder encoder;
    Cube_Core::SerialSender sender;
    Cube_Core::MotionPlanner planner;
    MLX90393_Driver::MLX90393 sensor;
    TMC429_Driver::TMC429 steppers;


    using p_pos = Cube_Common::Position_t*;
    using p_meas = Cube_Common::MeasurementData_t*;


    void sendReturn(Commands cmd, Errors err, uint32_t errCode, p_pos pos, p_meas meas) {
        auto returnMsg = encoder.encodeReturn(cmd, err, errCode, pos, meas);
        if (returnMsg) {
            sender.sendData(*returnMsg);
        }
    }
    
    std::optional<Cube_Common::EncodedData_t> doMove(Cube_Common::Command_t cmd);

    void moveInstruction(Cube_Common::Command_t cmd);
    void homeInstruction(void);
    void measureInstruction(void);
    void statusInstruction(void);
    
public:
    Reactor() :
        sender(Cube_Core::SerialSender(UART0_PERIPHERAL)),
        planner(Cube_Core::MotionPlanner(Cube_Config::planner)),
        sensor(MLX90393_Driver::MLX90393(Cube_Config::sensor)),
        steppers(TMC429_Driver::TMC429(Cube_Config::tmc429))
    {
        motorPower(0);
    }

    void doCommand(Cube_Common::Command_t cmd);
};