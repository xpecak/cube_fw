#pragma once

#include <optional>
#include <stdint.h>
#include "cube_common.hpp"
#include "pb_encode.h"
#include "protocols.pb.h"

namespace Cube_Core {

class SerialEncoder {
public:

    using opt_return = std::optional<Cube_Common::EncodedData_t>;
    using p_pos = Cube_Common::Position_t*;
    using p_meas = Cube_Common::MeasurementData_t*;

    opt_return encodeReturn(Commands cmd, Errors err, uint32_t errCode, p_pos pos, p_meas measure) const {
        Cube_Common::EncodedData_t output;
        Return_msg msg = Return_msg_init_zero;
        pb_ostream_t stream = pb_ostream_from_buffer(output.buffer.data(), output.buffer.max_size());
        msg.cmdAck = cmd;
        msg.error = err;
        msg.errorCode = errCode;
        if (pos != nullptr) {
            msg.has_position = true;
            msg.position.x = pos->x;
            msg.position.y = pos->y;
            msg.position.z = pos->z;
        }
        if (measure != nullptr) {
            msg.has_measurement = true;
            msg.measurement.x = measure->x;
            msg.measurement.y = measure->y;
            msg.measurement.z = measure->z;
        }
        if (!pb_encode(&stream, Return_msg_fields, &msg)) {
            return {};
        }
        output.dataLength = stream.bytes_written;
        return output;
    }
};

} // namespace Cube_Core