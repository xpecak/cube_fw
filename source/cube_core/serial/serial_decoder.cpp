#include "serial_decoder.hpp"
#include "cube_common.hpp"
#include "pb_decode.h"
#include "protocols.pb.h"

namespace Cube_Core {

void SerialDecoder::decodeBuffer(const Cube_Common::EncodedData_t& buffer) {
    Command_msg message = Command_msg_init_zero;
    Cube_Common::Command_t new_command;
    pb_istream_t stream = pb_istream_from_buffer(buffer.buffer.data(), buffer.dataLength);
    if (!pb_decode(&stream, Command_msg_fields, &message)) {
        new_command.instruction = Cube_Common::Instruction::raiseDecodeError;
    } else {
        switch(message.command) {
            case Commands_STATUS:
                new_command.instruction = Cube_Common::Instruction::status;
                break;
            case Commands_MOVETO:
                new_command.instruction = Cube_Common::Instruction::moveTo;
                break;
            case Commands_MEASURE:
                new_command.instruction = Cube_Common::Instruction::measure;
                break;
            case Commands_GET_ABS_POS:
                new_command.instruction = Cube_Common::Instruction::getAbsolutePosition;
                break;
            case Commands_GET_REL_POS:
                new_command.instruction = Cube_Common::Instruction::getRelativePosition;
                break;
            case Commands_HOME:
                new_command.instruction = Cube_Common::Instruction::home;
                break;
            case Commands_SET_ZERO_POS:
                new_command.instruction = Cube_Common::Instruction::setZeroPosition;
                break;
            default:
                new_command.instruction = Cube_Common::Instruction::unknown;
        }
        if (message.has_position) {
            Cube_Common::Position_t pos;
            pos.x = message.position.x;
            pos.y = message.position.y;
            pos.z = message.position.z;
            new_command.position = pos;
        }
    }
    parsedBuffer[parsedBufferWrite] = new_command;
    parsedBufferWrite++;
    if (parsedBufferWrite == PARSED_BUFFER_SIZE) {
        parsedBufferWrite = 0;
    }
}

Cube_Common::Command_t SerialDecoder::getCommand(void) {
    if (parsedBufferRead == parsedBufferWrite) {
        Cube_Common::Command_t nop_cmd;
        nop_cmd.instruction = Cube_Common::Instruction::nop;
        return nop_cmd;
    }
    else {
        uint32_t temp = parsedBufferRead;
        parsedBufferRead++;
        if (parsedBufferRead == PARSED_BUFFER_SIZE) {
            parsedBufferRead = 0;
        }
        return parsedBuffer[temp];
    }
}

} // namespace Cube_Core