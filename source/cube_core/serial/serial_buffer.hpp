#pragma once

#include <stdint.h>
#include <array>
#include "cube_common.hpp"

namespace Cube_Core {

constexpr uint32_t BUFFER_COUNT = 8;

enum class HeaderParseState {
    start, receivedFirst, receivedSecond, receivedThird, receivedLength, readingData
};

class SerialBuffer {
    std::array<Cube_Common::EncodedData_t, BUFFER_COUNT> buffers;
    uint32_t bufferWrite;
    uint32_t bufferRead;

    HeaderParseState parserState; 
    uint32_t bytesRead;
    
public:
    SerialBuffer() : bufferWrite(0U),
                        bufferRead(0U),
                        parserState(HeaderParseState::start),
                        bytesRead(0U)
    {}


    void addToBuffer(const uint8_t data) {
        switch (parserState) {
            case HeaderParseState::start:
                if (data == 0x55) {
                    parserState = HeaderParseState::receivedFirst;
                }
                break;
            case HeaderParseState::receivedFirst:
                if (data == 0x55) {
                    parserState = HeaderParseState::receivedSecond;
                } else {
                    parserState = HeaderParseState::start;
                }
                break;
            case HeaderParseState::receivedSecond:
                if (data == 0x55) {
                    parserState = HeaderParseState::receivedThird;
                } else {
                    parserState = HeaderParseState::start;
                }
                break;
            case HeaderParseState::receivedThird:
                buffers[bufferWrite].dataLength = data;
                bytesRead = 0;
                parserState = HeaderParseState::receivedLength;
                break;
            case HeaderParseState::receivedLength:
                buffers[bufferWrite].buffer[bytesRead] = data;
                bytesRead++;
                if (bytesRead == buffers[bufferWrite].dataLength) {
                    bufferWrite++;
                    if (bufferWrite == BUFFER_COUNT) {
                        bufferWrite = 0;
                    }
                    parserState = HeaderParseState::start;
                }
                break;
            default:
                parserState = HeaderParseState::start;
                break;
        }
    }

    const bool bufferReady() const {
        return bufferWrite != bufferRead;
    }

    const Cube_Common::EncodedData_t& getBuffer() {
        Cube_Common::EncodedData_t& temp = buffers[bufferRead];
        bufferRead++;
        if (bufferRead == BUFFER_COUNT) {
            bufferRead = 0;
        }
        return temp;
    }
};
} // namespace Cure_Core