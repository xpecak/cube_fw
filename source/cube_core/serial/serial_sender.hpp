#pragma once

#include <stdint.h>
#include "cube_common.hpp"
#include "MK64F12.h"
#include "fsl_uart.h"

namespace Cube_Core {

class SerialSender {
    UART_Type* UART_peripheral;
    public:
    SerialSender(UART_Type* uart) : UART_peripheral(uart) {}
    void sendData(const Cube_Common::EncodedData_t& data) const {
        uint8_t header[4] = {0x55, 0x55, 0x55, data.dataLength};
        UART_WriteBlocking(UART_peripheral, header, 4);
        UART_WriteBlocking(UART_peripheral, data.buffer.begin(), data.dataLength);
    }
};

} //namespace Cube_Core