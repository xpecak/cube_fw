#pragma once

#include <stdint.h>
#include "cube_common.hpp"

namespace Cube_Core {

    constexpr uint32_t PARSED_BUFFER_SIZE = 32U;

    class SerialDecoder {

        Cube_Common::Command_t parsedBuffer[PARSED_BUFFER_SIZE];
        uint32_t parsedBufferWrite;
        uint32_t parsedBufferRead;

    public:
        SerialDecoder() : parsedBufferWrite(0U), parsedBufferRead(0U) {}
        void decodeBuffer(const Cube_Common::EncodedData_t& buffer);
        Cube_Common::Command_t getCommand(void);
    };
} //namespace Cube_Core