#pragma once

#include <stdint.h>
#include <optional>

#include "cube_common.hpp"

namespace Cube_Core {

struct PlannerConfig_t {
    uint32_t stepResolutionXY;
    uint32_t stepResolutionZ;
    float sizeX;
    float sizeY;
    float sizeZ;
};

enum class PlannerErrors {
    noError, outOfBounds
};

struct PlannerResult_t {
    PlannerErrors error;
    std::optional<Cube_Common::Steps_t> steps;
    std::optional<Cube_Common::Steps_t> relSteps;
};

class MotionPlanner {
    PlannerConfig_t config;
    Cube_Common::Position_t currentPosition;
    Cube_Common::Position_t zeroPosition;

public:
    MotionPlanner(const PlannerConfig_t& conf) : config(conf),
                              currentPosition({0, 0, 0}),
                              zeroPosition({0, 0, 0}) 
    {}


    void changeConfig(PlannerConfig_t& newConf) {
        config = newConf;
    }

    Cube_Common::Position_t getAbsolutePosition(void) const {
        return currentPosition;
    }

    Cube_Common::Position_t getRelativePosition(void) const {
        return currentPosition - zeroPosition;
    }

    void resetPosition(void) {
        zeroPosition = {0, 0, 0};
        currentPosition = {0, 0, 0};
    }

    void setZeroPosition(void) {
        zeroPosition = currentPosition;
    }

    void setPosition(Cube_Common::Position_t& position) {
        currentPosition = position;
    }
    
    PlannerResult_t moveTo(Cube_Common::Position_t& position);
};

}   //namespace Cube_Core



