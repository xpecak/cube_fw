#include <stdint.h>
#include <optional>

#include "planner.hpp"

namespace Cube_Core {

using Pos_t = Cube_Common::Position_t;

PlannerResult_t MotionPlanner::moveTo(Cube_Common::Position_t& position) {
    Pos_t goalPos = zeroPosition + position;

    Pos_t relPos = goalPos - currentPosition;
    
    if (goalPos.x < 0 || goalPos.y < 0 || goalPos.z < 0) {
        return {PlannerErrors::outOfBounds, {}, {}};
    }
    if (goalPos.x > config.sizeX || goalPos.y > config.sizeY || goalPos.z > config.sizeZ) {
        return {PlannerErrors::outOfBounds, {}, {}};
    }

    Cube_Common::Steps_t steps;
    steps.a = 0.5 * (goalPos.x + goalPos.y) * config.stepResolutionXY;
    steps.b = 0.5 * (goalPos.x - goalPos.y) * config.stepResolutionXY;
    steps.z = goalPos.z * config.stepResolutionZ;

    Cube_Common::Steps_t relSteps;
    relSteps.a = 0.5 * (relPos.x + relPos.y) * config.stepResolutionXY;
    relSteps.b = 0.5 * (relPos.x - relPos.y) * config.stepResolutionXY;
    relSteps.z = relPos.z * config.stepResolutionZ;

    currentPosition = goalPos;
    
    return {PlannerErrors::noError, steps, relSteps};
}

} //namespace Cube_Core