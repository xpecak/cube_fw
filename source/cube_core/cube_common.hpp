#pragma once

#include <optional>
#include <array>

namespace Cube_Common {

enum class Instruction {
    status, moveTo, measure, 
    getRelativePosition, getAbsolutePosition, 
    setZeroPosition, home, nop, unknown,
    raiseDecodeError
};

struct Position_t {
    float x;
    float y;
    float z;

    Position_t& operator+=(const Position_t& rhs) {
        x += rhs.x;
        y += rhs.y;
        z += rhs.z;
        return *this;
    }

    Position_t& operator-=(const Position_t& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        z -= rhs.z;
        return *this;
    }
};

Position_t operator+(Position_t lhs, const Position_t& rhs) {
    lhs += rhs;
    return lhs;
}

Position_t operator-(Position_t lhs, const Position_t& rhs) {
    lhs -= rhs;
    return lhs;
}


struct Steps_t {
    int32_t a;
    int32_t b;
    int32_t z;
};

struct MeasurementData_t {
    float x;
    float y;
    float z;
    float t;
};

struct Command_t {
    Instruction instruction;
    std::optional<Position_t> position;
};

constexpr uint32_t BUFFER_SIZE = 64U;
using buffer_t = std::array<uint8_t, BUFFER_SIZE>;

struct EncodedData_t {
    buffer_t buffer;
    uint32_t dataLength;
};

} //namespace Cube_common

