#pragma once

#include "MLX90393.hpp"
#include "TMC429.hpp"
#include "planner.hpp"
#include "peripherals.h"
#include "fsl_dspi.h"

TMC429_Driver::Status SPITransfer(uint8_t* tx, uint8_t *rx, uint8_t length) {
    dspi_transfer_t transfer;
    transfer.rxData = rx;
    transfer.txData = tx;
    transfer.dataSize = length;
    transfer.configFlags = kDSPI_MasterCtar0 | kDSPI_MasterPcsContinuous;

    status_t res = DSPI_MasterTransferBlocking(SPI0_PERIPHERAL, &transfer);
    if (res != kStatus_Success) {
        return TMC429_Driver::Status::fail;
    }
    return TMC429_Driver::Status::success;
}

namespace Cube_Config {

    constexpr int16_t XY_HOMING = 100;
    constexpr int16_t Z_HOMING = 250;

    Cube_Core::PlannerConfig_t planner = {
        .stepResolutionXY = 400,
        .stepResolutionZ = 3200,
        .sizeX = 234.0,
        .sizeY = 275.0,
        .sizeZ = 270.0
    };

    MLX90393_Driver::Config sensor = {
        .I2C_peripheral = I2C0_PERIPHERAL,
        .address = 0x0C,
        .gain = 7U,
        .resolution = 0U,
        .OSR = 1U,
        .digi_filter = 7U,
        .temp_comp = 0U,
        .t_ref = 0U,
        .measure_delay = 750000,
    };

    TMC429_Driver::Config tmc429 = {
        .transfer = SPITransfer,
        .steppers = {
            {  // Stepper 1
                .v_min = 1U,
                .v_max = 1573U,
                .a_max = 1320U,
                .pmul = 163U,
                .pdiv = 6U,
                .ramp_div = 9U,
                .pulse_div = 4U,
            },
            {  // Stepper 2
                .v_min = 1U,
                .v_max = 1573U,
                .a_max = 1320U,
                .pmul = 163U,
                .pdiv = 6U,
                .ramp_div = 9U,
                .pulse_div = 4U,
            },
            {  // Stepper 3
                .v_min = 1U,
                .v_max = 1573U,
                .a_max = 1320U,
                .pmul = 163U,
                .pdiv = 7U,
                .ramp_div = 9U,
                .pulse_div = 3U,
            }
        }
    };
} //namespace Cube_Config