#include "fsl_device_registers.h"
#include "board.h"
#include "pin_mux.h"
#include "peripherals.h"
#include "clock_config.h"
#include "fsl_uart.h"


#include "serial_decoder.hpp"
#include "serial_buffer.hpp"
#include "cube_common.hpp"
#include "reactor.hpp"


Cube_Core::SerialBuffer uart_buffer{};

extern "C" {

void UART0_IRQHandler(void) {
    //we need to clear the RDRF flag by reading S1 register first
    //the SDK doesnt do it properly, and it is mentioned in single line in 2k pages of documentation
    uint8_t temp = UART0_PERIPHERAL->S1;
    uart_buffer.addToBuffer(UART_ReadByte(UART0_PERIPHERAL));
}

} // extern "C"

/*
Entry point
*/
int main(void)
{
    // Init board hardware.
    BOARD_InitBootPins();
    BOARD_InitBootPeripherals();
    BOARD_BootClockRUN();

    Cube_Core::SerialDecoder decoder{};
    Reactor reactor{};

    while (1)
    {

        if (uart_buffer.bufferReady()) {
            decoder.decodeBuffer(uart_buffer.getBuffer());
        }
        
        Cube_Common::Command_t cmd = decoder.getCommand();
        reactor.doCommand(cmd);
    }
}
