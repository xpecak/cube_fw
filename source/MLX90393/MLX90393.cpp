#include "MLX90393.hpp"

#include "MK64F12.h"
#include "fsl_common.h"
#include "fsl_i2c.h"
#include <stdint.h>

namespace {

void delay(int counter) {
	for (int i = 0; i < counter; i++) {
		__asm volatile ("nop");
	}
}

} //namespace


namespace MLX90393_Driver {

MLX90393::MLX90393(const Config& config) :
    config(config),
    configValid(true)
{
    if (uploadConfig() != Status::success) {
        configValid = false;
    }
    updateSensitivity();
}


Status MLX90393::i2cTransfer(const uint8_t* const tx_data, const uint8_t tx_length,
							   uint8_t* rx_data, const uint8_t rx_length)
{
    status_t result;
    result = I2C_MasterStart(config.I2C_peripheral, config.address, kI2C_Write);
    if (result != kStatus_Success) {
        return Status::fail;
    }
    delay(1200);
    result = I2C_MasterWriteBlocking(config.I2C_peripheral, tx_data, tx_length, kI2C_TransferNoStopFlag);
    if (result != kStatus_Success) {
        return Status::fail;
    }
    delay(1200);
    result = I2C_MasterRepeatedStart(config.I2C_peripheral, config.address, kI2C_Read);
    if (result != kStatus_Success) {
        return Status::fail;
    }
    delay(1200);
    result = I2C_MasterReadBlocking(config.I2C_peripheral, rx_data, rx_length, kI2C_TransferDefaultFlag);
    if (result != kStatus_Success) {
        return Status::fail;
    }
    delay(1200);
    if (rx_data[0] & STATUS_ERROR_MASK) {
        return Status::fail;
    }
    return Status::success;
}

Status MLX90393::writeRegister(const uint8_t addr, const uint16_t data)
{
    uint8_t command[4] = {0b01100000,
                                (data >> 8) & 0xff,
                                data & 0xff,
                                addr << 2};
    uint8_t status_answer;
    return i2cTransfer(command, 4, &status_answer, 1);
}
Status MLX90393::readRegister(const uint8_t addr, uint16_t* data)
{
    uint8_t command[2] = {0b01010000, addr << 2};
    uint8_t received_data[3] = {0, 0, 0};
    Status result = i2cTransfer(command, 2, received_data, 3);
    *data = (received_data[1] << 8) + received_data[2];
    return result;
}

Status MLX90393::uploadConfig()
{
    uint16_t registers[3] = {0, 0, 0};
    Status result;

    result = readRegister(0, registers);
    if (result != Status::success) {
        return result;
    }

    uint16_t manu_config = registers[0] & 0xff00;
    registers[0] = manu_config | (config.gain << GAIN_OFFSET) | 0xC;
    registers[1] = 0b0000000000100011
    			 | config.temp_comp << TEMP_COMP_OFFSET;
    registers[2] = config.OSR << OSR_OFFSET
                 | config.digi_filter << DIGI_FILTER_OFFSET
                 | config.resolution << RESOLUTION_OFFSET
                 | config.resolution << (RESOLUTION_OFFSET + 2)
                 | config.resolution << (RESOLUTION_OFFSET + 4)
                 | 0 << OSR2_OFFSET;

    for (int i = 0; i < 3; i++) {
        result = writeRegister(i, registers[i]);
        if (result != Status::success) {
            return result;
        }
    }
    return result;
}

Status MLX90393::triggerMeasurement(const uint8_t flags)
{
    uint8_t command = 0b00110000 | flags;
    uint8_t status_answer;
    return i2cTransfer(&command, 1, &status_answer, 1);
}

RawResult MLX90393::getMeasurement()
{
    RawResult result;
    uint8_t command = 0b01001111;
    uint8_t read_length = 9;
    uint8_t data[9];
    result.result_status = i2cTransfer(&command, 1, data, read_length);
    if (result.result_status != Status::success) {
        return result;
    }

    result.result_status = readRegister(0x24, &result.t_ref);
    if (result.result_status != Status::success) {
        return result;
    }

    result.t = (data[1] << 8) | data[2];
    result.x = (data[3] << 8) | data[4];
    result.y = (data[5] << 8) | data[6];
    result.z = (data[7] << 8) | data[8];
    return result;
}

void MLX90393::measureDelay(void)
{
    delay(config.measure_delay);
}

Result MLX90393::convertRawResult(const RawResult data)
{
	Result result;
	result.t = 35.f + (((float) (data.t - data.t_ref))/45.2);
	switch (config.resolution) {
	case 0:
	case 1:
        //This should not work at all, wtf.
		result.x = ((int16_t) data.x) * sensitivity.xy;
		result.y = ((int16_t) data.y) * sensitivity.xy;
		result.z = ((int16_t) data.z) * sensitivity.z;
		break;
	case 2:
		result.x = (data.x - 32768.f) * sensitivity.xy;
		result.y = (data.y - 32768.f) * sensitivity.xy;
		result.z = (data.z - 32768.f) * sensitivity.z;
		break;
	case 3:
		result.x = (data.x - 16384.f) * sensitivity.xy;
		result.y = (data.y - 16384.f) * sensitivity.xy;
		result.z = (data.z - 16384.f) * sensitivity.z;
		break;
	}
	return result;
}


Status MLX90393::changeConfig(const Config& new_config)
{
    config = new_config;
    Status result = uploadConfig();
    if (result != Status::success) {
        configValid = false;
    }
    return result;
}

} //namespace MLX90393_Driver
