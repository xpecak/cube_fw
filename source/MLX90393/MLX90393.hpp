/*
 * MLX90393_driver.h
 *
 *  Created on: Nov 13, 2019
 *      Author: loady
 */

#pragma once
#include <stdint.h>
#include "fsl_common.h"
#include "MK64F12.h"

namespace MLX90393_Driver {


//Conversion flags
constexpr uint8_t MEASUREMENT_X_FLAG = 0b00000010;
constexpr uint8_t MEASUREMENT_Y_FLAG = 0b00000100;
constexpr uint8_t MEASUREMENT_Z_FLAG = 0b00001000;
constexpr uint8_t MEASUREMENT_T_FLAG = 0b00000001;


//Register masks, offsets
constexpr uint16_t GAIN_MASK = 0b0000000001110000;
constexpr uint8_t GAIN_OFFSET = 4;

constexpr uint16_t RESOLUTION_MASK = 0b0000011111100000;
constexpr uint8_t RESOLUTION_OFFSET = 5;

constexpr uint16_t OSR_MASK = 0b0000000000000011;
constexpr uint8_t OSR_OFFSET = 0;

constexpr uint16_t OSR2_MASK = 0b0001100000000000;
constexpr uint8_t OSR2_OFFSET = 11;

constexpr uint16_t DIGI_FILTER_MASK = 0b0000000000011100;
constexpr uint8_t DIGI_FILTER_OFFSET = 2;

constexpr uint16_t TEMP_COMP_MASK = 0b0000000000000100;
constexpr uint8_t TEMP_COMP_OFFSET = 2;

//Status error mask
constexpr uint8_t STATUS_ERROR_MASK = 0b00010000;

// xy sens LUT
constexpr float xy_sens_LUT[3][8] =
{
{0.751, 0.601, 0.451, 0.376, 0.300, 0.250, 0.200, 0.150},
{1.502, 1.202, 0.901, 0.751, 0.601, 0.501, 0.401, 0.300},
{3.004, 2.403, 1.803, 1.502, 1.202, 1.001, 0.801, 0.601}
};

// z sens LUT
constexpr float z_sens_LUT[3][8] =
{
{1.210, 0.968, 0.726, 0.605, 0.484, 0.403, 0.323, 0.242},
{2.420, 1.936, 1.452, 1.210, 0.968, 0.807, 0.645, 0.484},
{4.840, 3.872, 2.904, 2.420, 1.936, 1.613, 1.291, 0.968}
};


enum class Status {
	success,
	fail
};

struct Config {
	I2C_Type* I2C_peripheral;
	uint8_t address;
	uint8_t gain;
	uint8_t resolution;
	uint8_t OSR;
	uint8_t digi_filter;
	uint8_t temp_comp;
	uint16_t t_ref;
	int measure_delay;
};

struct RawResult {
	Status result_status;
	uint16_t x;
	uint16_t y;
	uint16_t z;
	uint16_t t;
	uint16_t t_ref;
};

struct Result {
	float x;
	float y;
	float z;
	float t;
};

struct Sensitivity {
	float xy;
	float z;
};

class MLX90393 {
	Sensitivity sensitivity;
	Config config;

	Status i2cTransfer(const uint8_t* const tx_data, const uint8_t tx_length,
					   uint8_t* rx_data, const uint8_t rx_length);
	Status writeRegister(const uint8_t addr, const uint16_t data);
	Status readRegister(const uint8_t addr, uint16_t* data);
	Status uploadConfig();

	void updateSensitivity() {
		sensitivity.xy = xy_sens_LUT[config.resolution][config.gain];
    	sensitivity.z = z_sens_LUT[config.resolution][config.gain];
	}
	

public:
	bool configValid;
	MLX90393(const Config& config);
	Status triggerMeasurement(const uint8_t flags);
	RawResult getMeasurement();
	void measureDelay(void);
	Result convertRawResult(const RawResult data);
	Status changeConfig(const Config& new_config);

	Sensitivity getSensitivity() const
	{
		return sensitivity;
	}

	Config getConfig() const
	{
		return config;
	}
};


} // namespace MLX90393_Driver