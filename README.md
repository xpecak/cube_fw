# Cube's Firmware

This repo contains the controller firmware for ___Cube___, a 3D magnetic field scanning machine that is currently being developed.

The firmware is licensed under GNU LGPLv3, full license in `LICENSE.md`.
The MCUXpresso SDK is licensed with BSD 3-Clause.

## Repository structure
`\` contains the build scripts and this `README.md` file

`\source\` contains the actual firmware source code.

`\sdk\` contains the MCUXpresso SDK files and associated `.mex` file for the MCUXpresso Config Tool.

## Building
Building is currently tested and working in Ubuntu 19.04 and WSL.

#### Build requirements:
 
 - Cmake
 - make
 - Python
 - `protobuf-compiler`, `python-protobuf`, `nanopb` python packages
 - [gcc-arm-none-eabi](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)

#### Building

Before first build, you need to edit few paths in the scripts. In `armgcc.cmake`, you need to specify your `gcc-arm-none-eabi` toolchain path. 

Launch `build_all.sh`, `build_debug.sh` or `build_release.sh`. 

`build_proto.sh` re-generates the Protocol Buffers `.h` and `.c` files. Unless you are building with `build_all.sh`, you need to launch it separately after every edit to `protocols.proto` file.

## Making changes to MCU configuration

To make changes in MCU configuration(pins, peripherals, clocks), please use [MCUXpresso Config Tools](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools/mcuxpresso-config-tools-pins-clocks-peripherals:MCUXpresso-Config-Tools). The configuration is stored in `\sdk\Cube_FW.mex`.

## TODO:
 1. MLX90393 - fix temperature compensation
 1. rewrite MLX90393 driver so that it is fully SDK/platform independent
 1. properly fix the CmakeLists.txt, not just visual fixes
 1. Refactor code (missing const, pointers -> references, std::array, make it more C++)